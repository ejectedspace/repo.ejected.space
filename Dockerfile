FROM node
WORKDIR /app
COPY ./ /app/

RUN npm install
RUN npm run-script build

FROM nginx:1.15.5-alpine
COPY --from=0 /app/dist /usr/share/nginx/html

